<?php

namespace model;

class StoreModel {

  static function listCategories(): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }

  static function listProducts(): array
  {
      // Connexion à la base de données
      $db = \model\Model::connect();

      // Requête SQL
      $sql = "SELECT p.id as id_produit , p.name as name_produit, p.price as price_produit, p.image as img_produit, c.name as name_category FROM product as p INNER JOIN category as c ON (p.category=c.id)";

      // Exécution de la requête
      $req = $db->prepare($sql);
      $req->execute();

      // Retourner les résultats (type array)
      return $req->fetchAll();
  }
    static function infoProduct(int $id){
        $db1 = \model\Model::connect();
        // Requête SQL
        $sql1 = "SELECT p.name as name_produit, p.price as price_produit, p.image as img_produit, p.spec as spec_produit, c.name as name_category FROM product as p INNER JOIN category as c ON (p.category=c.id) WHERE  p.id = $id";
        if($sql1==null){
            header("Location: /store.php");
            exit();
        }
        else{
            // Exécution de la requête
            $req1 = $db1->prepare($sql1);
            $req1->execute();

            // Retourner les résultats (type array)
            return $req1->fetch();
        }
    }


}