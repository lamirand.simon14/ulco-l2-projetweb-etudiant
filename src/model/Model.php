<?php

namespace model;

class Model {

  static function connect()
  {
    $dsn = "mysql:host=localhost:3307;dbname=webstore";
    $user = "root";
    $pass = "";
    return new \PDO($dsn, $user, $pass);
  }

}