<div id="store">

<!-- Filtrer l'affichage des produits  ---------------------------------------->

<form>

  <h4>Rechercher</h4>
  <input type="text" name="search" placeholder="Rechercher un produit" />

  <h4>Catégorie</h4>
  <?php foreach ($params["categories"] as $c) { ?>
    <input type="checkbox" name="category[]" value="<?= $c["name"] ?>" />
    <?= $c["name"] ?>
    <br/>
  <?php } ?>

  <h4>Prix</h4>
  <input type="radio" name="order" /> Croissant <br />
  <input type="radio" name="order" /> Décroissant <br />

  <div><input type="submit" value="Appliquer" /></div>

</form>

<!-- Affichage des produits --------------------------------------------------->

<div class="products">

<!-- TODO: Afficher la liste des produits ici -->
    <?php
    foreach($params['product'] as $tmp)
    {
        ?>
        <div class="card">
            <p class="card-image">
                <img src="/public/images/<?php echo $tmp['img_produit'] ?>">
            </p>
            <p class="card-category">
                <?php echo $tmp['name_category'] ?>
            </p>
            <p class="card-title">
                <a href="/store/<?php echo$tmp['id_produit']?>">
                    <?php echo $tmp['name_produit'] ?>
                </a>
            </p>
            <p class="card-price">
                <?php echo $tmp['price_produit'] ?>
            </p>
        </div>
    <?php } ?>

</div>

</div>
