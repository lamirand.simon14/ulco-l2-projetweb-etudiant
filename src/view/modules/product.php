<div id="product">
    <?php $cmp=$params['info'];
    ?>
    <div>
        <div class="product-images">
            <img src="/public/images/<?= $cmp['img_produit']?>" id="img"/>
            <div class="product-miniatures">
                <div>
                    <img src="/public/images/<?= $cmp['img_produit']?>" class="pic"/>
                </div>
                <?php for($i=1;$i<=3;$i++):?>
                    <div>
                        <img src="/public/images/<?=str_replace(".jpg","_alt$i.jpg",$cmp['img_produit'])?>" class="pic"/>
                    </div>
                <?php endfor;?>
            </div>
        </div>

        <div class="product-infos">
            <p class="product-category">
                <?php echo $cmp['name_category'] ?>
            </p>
            <h1>
                <?php echo $cmp['name_produit'] ?>
            </h1>
            <p class="product-price">
                <?php echo $cmp['price_produit'] ?>
            </p>
            <form>
                <button type="button" class="bouton-moins">-</button>
                <button type="button" id="nombre">1</button>
                <button type="button" class="bouton-plus">+</button>
                <input type="submit" value="Ajouter au panier"/>
            </form>
            <div class="box error" style="visibility:hidden" id="box">
                Quantité maximale autorisée !
            </div>
        </div>
    </div>
    <div>
        <div class="product-spec">
            <h2>Spécifités</h2>
            <?php echo $cmp['spec_produit'] ?>
        </div>
        <div class="product-comments">
            <h2>Avis</h2>
            <p>Il n'y a pas d'avis pour ce produit.</p>
        </div>
    </div>
</div>
<script src="/public/scripts/product.js"></script>

