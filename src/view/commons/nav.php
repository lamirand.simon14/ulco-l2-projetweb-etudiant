<nav>
    <img src="/public/images/logo.jpeg" />
    <a href="/">
        Accueil
    </a>
    <a href="/store">
        Boutique
    </a>
    <a class="account" href="/account">
        <img src="/public/images/avatar.png" />
        Compte
    </a>
</nav>
